package de.thm.ds.foregroundserviceexercise;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Calendar;
import java.util.Date;

public class ClockForegroundService extends IntentService {
    private static final String NOTIFICATION_CHANNEL = "ClockForegroundServiceChannel";
    private static final int ONGOING_NOTIFICATION_ID = 1;

    private Thread workerThread;
    private boolean isRunning;

    private NotificationCompat.Builder notificationBuilder;
    private boolean firstTimeBuild = true;
    private Handler handler;

    public ClockForegroundService() {
        super(ClockForegroundService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        prepareAndStartForeground();

        workerThread = Thread.currentThread();
        isRunning = true;
        long lastMilliseconds = System.currentTimeMillis();
        int lastMinute = Calendar.getInstance().get(Calendar.MINUTE);

        while (isRunning) {
            long currentMilliseconds = System.currentTimeMillis();
            if(currentMilliseconds - lastMilliseconds > 10 * 1000) {
                startForeground(ONGOING_NOTIFICATION_ID, getNotification());
                lastMilliseconds = currentMilliseconds;
            }

            Calendar calendar = Calendar.getInstance();
            int currentMinute = calendar.get(Calendar.MINUTE);
            if (currentMinute != lastMinute) {
                lastMinute = currentMinute;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), Calendar.getInstance().getTime().toString(), Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }
        }
    }

    private void prepareAndStartForeground() {
        createNotificationChannel();
        startForeground(ONGOING_NOTIFICATION_ID, getNotification());
    }

    private Notification getNotification() {
        final Date currentTime = Calendar.getInstance().getTime();
        final String text = currentTime.toString();

        if (firstTimeBuild) {
            notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                    .setContentTitle("Uhrzeit")
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setOnlyAlertOnce(true);

            firstTimeBuild = false;
        }

        notificationBuilder.setContentText(text);

        return notificationBuilder.build();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL,
                    "Uhrzeit", NotificationManager.IMPORTANCE_DEFAULT);
            getSystemService(NotificationManager.class).createNotificationChannel(channel);
        }
    }


    @Override
    public void onDestroy() {
        isRunning = false;
        if (workerThread != null) workerThread.interrupt();
        stopForeground(true);
        super.onDestroy();
    }
}